class Taxer {

    private final double taxRate;

    Taxer(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public double tax(double value) {
        return value * (1 + taxRate);
    }
}