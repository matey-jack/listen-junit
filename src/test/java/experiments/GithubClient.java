package experiments;

import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class GithubClient {

    @Test
    public void main() throws IOException {
        ObjectMapper mapper = new ObjectMapper()
                .configure(Feature.IGNORE_UNKNOWN, true)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        Call<List<Repo>> listCall = service.listRepos("matey-jack");

        Response<List<Repo>> response = listCall.execute();

        System.out.println(response);
        final String names = response.body().stream()
                .map(Repo::getName)
                .collect(Collectors.joining(", "));
        System.out.println(names);
    }
}
