package ogim.xray;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface XRayApi {

    @POST("authenticate")
    Call<String> authenticate(@Body AuthInput input);

    @POST("/api/v1/import/execution")
    Call<ImportResponse> importExecution(@Header("Authorization") String auth, @Body TestExecution execution);

}
