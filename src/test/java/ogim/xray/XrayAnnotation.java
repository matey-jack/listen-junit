package ogim.xray;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention (RetentionPolicy.RUNTIME)
public @interface XrayAnnotation {
    String xrayKey();
    String repositoryPath();
    int currentRevision() default 1;
    //String[] testSets() default {"", ""};
}
