package ogim.xray;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.lang.annotation.Annotation;


public class XrayListener extends RunListener {

    private static final XrayClient client = new XrayClient();

    private Description lastFailed = null;

    public void getXrayTestplan() {

    }

    @Override
    public void testRunStarted(Description description) throws Exception {
        System.out.println("hoho, let's go");
    }

    @Override
    public void testRunFinished(Result result) throws Exception {
        // TODO: anything we need to do here? write an email to the project manager maybe?
        //        or a message to Slack?
    }

    @Override
    public void testStarted(Description description) throws Exception {
        System.out.println("Now running: " + description.getDisplayName());
    }

    @Override
    public void testFinished(Description description) throws Exception {
        // document that stateful madness caused by JUnits weird interfaces.

        Class testClass = description.getTestClass();
        XrayAnnotation xrayAnnotation =
                (XrayAnnotation) testClass.getAnnotationsByType(XrayAnnotation.class)[0];
        //xrayAnnotation.repositoryPath()
        boolean failed = description.equals(lastFailed);
        String result = failed ? "failed" : "succeeded";
        System.out.println("test " + description.getDisplayName() + " has " + result + ".");

        client.putTestResult(description.getDisplayName(), !failed);
    }

    @Override
    public void testFailure(Failure failure) throws Exception {
        lastFailed = failure.getDescription();

    }

    @Override
    public void testIgnored(Description description) throws Exception {
        // TODO: check if 'testFinished' is also called for ignored tests
        //       in that case, we won't need this method

        // Same would be true for 'testAssumptionFailure', but most people aren't using
        // testAssumptions anyways...
    }
}
