package ogim.xray;

public class ImportResponse {
    // either 'error' or all the other fields are set
    public String error;

    public String id;
    public String key;
    public String self;

    @Override
    public String toString() {
        return "ImportResponse{" +
                "error='" + error + '\'' +
                ", id='" + id + '\'' +
                ", key='" + key + '\'' +
                ", self='" + self + '\'' +
                '}';
    }
}
