package ogim.xray;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static java.lang.System.out;

public class ClassDeclarationSpy {
    private static final String  fmt = "%24s: %s%n";

    public static void main(String... args) {
        System.out.println("Starting main");
        try {
            Class<?> c = Class.forName("TaxerTest");

            out.format("Class:%n  %s%n%n", c.getCanonicalName());
            out.format("Modifiers:%n  %s%n%n",
                    Modifier.toString(c.getModifiers()));

            Annotation[] ann = c.getAnnotations();
            if (ann.length != 0) {
                for (Annotation a : ann) {
                    Class<?> aClass = a.annotationType();
                    out.format("  %s, class: %s%n", a.toString(), aClass.getName());
                    if (a instanceof XrayAnnotation) {
                        XrayAnnotation xrayAnn = (XrayAnnotation) a;
                        out.format("  Xray key: %s", xrayAnn.xrayKey());
                        out.format("  Repo path: %s", xrayAnn.repositoryPath());
                    }
                }
                out.format("%n");
            } else {
                out.format("  -- No Annotations --%n%n");
            }

            Method[] allMethods = c.getDeclaredMethods();
            for (Method m : allMethods) {

                out.format("%s%n", m.toGenericString());

                out.format(fmt, "ReturnType", m.getReturnType());
                //out.format(fmt, "GenericReturnType", m.getGenericReturnType());

                Annotation[] mAnn = m.getAnnotations();
                if (ann.length != 0) {
                    for (Annotation a : mAnn) {
                        Class<?> aClass = a.annotationType();
                        out.format("  %s, class: %s%n", a.toString(), aClass.getName());
                        if (a instanceof XrayAnnotation) {
                            XrayAnnotation xrayAnn = (XrayAnnotation) a;
                            out.format("  Xray key: %s", xrayAnn.xrayKey());
                            out.format("  Repo path: %s%n", xrayAnn.repositoryPath());
                        }
                    }
                    out.format("%n");
                } else {
                    out.format("  -- No Annotations --%n%n");
                }

                /*Class<?>[] pType  = m.getParameterTypes();
                Type[] gpType = m.getGenericParameterTypes();
                for (int i = 0; i < pType.length; i++) {
                    out.format(fmt,"ParameterType", pType[i]);
                    out.format(fmt,"GenericParameterType", gpType[i]);
                }

                Class<?>[] xType  = m.getExceptionTypes();
                Type[] gxType = m.getGenericExceptionTypes();
                for (int i = 0; i < xType.length; i++) {
                    out.format(fmt,"ExceptionType", xType[i]);
                    out.format(fmt,"GenericExceptionType", gxType[i]);
                } */
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
