package ogim.xray;

import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class XrayClient {
    private final Properties properties = new Properties();
    private final ObjectMapper mapper = new ObjectMapper()
            .configure(Feature.IGNORE_UNKNOWN, true)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(" https://xray.cloud.xpand-it.com/api/v1/")
            .addConverterFactory(JacksonConverterFactory.create(mapper))
            .build();

    private final XRayApi xrayApi = retrofit.create(XRayApi.class);

    // TODO: use a system property
    private static final String xrayTestPlanKey = "XI-2";

    private final Map<String, String> xrayTestKeys = new HashMap<>();

    private final String auth;

    public XrayClient() {
        // TODO: for CI (Jenkins) usage, load the credentials from an environment variable
        try {
            properties.load(new FileInputStream(new File("secret")));
        } catch (IOException e) {
            throw new RuntimeException("Where's my 'secret'??", e);
        }

        AuthInput authInput = new AuthInput();
        authInput.client_id = (String) properties.get("id");
        authInput.client_secret = (String) properties.get("secret");

        try {
            Response<String> response = xrayApi.authenticate(authInput).execute();
            auth = "Bearer " + response.body();
        } catch (IOException e) {
            throw new RuntimeException("Can't authenticate :-( ", e);
        }

        // TODO: read from test plan Jira response
        xrayTestKeys.put("test(TaxerTest)", "XI-1");
        xrayTestKeys.put("otherTest(TaxerTest)", "XI-4");
    }

    public String getTestKey(String testName) {
        return xrayTestKeys.get(testName);
    }

    public void putTestResult(String testName, boolean success) throws IOException {
        TestExecution execution = new TestExecution();
        execution.info.testPlanKey = xrayTestPlanKey;
        TestResult result = new TestResult();
        result.start = "2014-08-30T11:50:56+01:00"; // new Date().toString();
        result.finish = "2014-08-30T11:50:56+01:00"; // new Date().toString();
        result.status = success ? "PASSED" : "FAILED";
        result.testKey = xrayTestKeys.get(testName);
        execution.tests.add(result);

        Response<ImportResponse> response = xrayApi.importExecution(auth, execution).execute();
        if (response.isSuccessful()) {
            System.out.println(response.body());
        } else {
            System.out.println(response.code() + ": " + response.errorBody().string());
            System.out.println(mapper.writeValueAsString(execution));
        }
    }

    @Test
    public void main() throws IOException {
    }
}
