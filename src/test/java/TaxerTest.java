import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Offset.offset;
import ogim.xray.XrayAnnotation;

//@SuppressWarnings("test annotation")
//@Deprecated

@XrayAnnotation (
        xrayKey = "QSAVTEST-16",
        repositoryPath = "/Regression"
)
public class TaxerTest extends TestBase {


    Taxer taxer = new Taxer(0.15);

    @XrayAnnotation (
            xrayKey = "QSAVTEST-1555",
            repositoryPath = "/Regression/Taxer"
    )
    @Test
    public void test() {
        assertThat(taxer.tax(20)).isCloseTo(23.0, offset(0.01));
    }

    @Test
    public void otherTest() {

        assertThat(taxer.tax(777)).isCloseTo(777.0, offset(0.01));
    }


}